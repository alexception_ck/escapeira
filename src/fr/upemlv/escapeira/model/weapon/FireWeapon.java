package fr.upemlv.escapeira.model.weapon;

import org.jbox2d.common.Vec2;

import fr.upemlv.escapeira.model.element.Bullet;

import fr.upemlv.escapeira.model.element.FireBullet;


/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class FireWeapon extends AbstractWeapon {
    public static final String ID = "fire";

    public FireWeapon(int category, int ammo) {
        super(FireWeapon.ID, category, ammo);
    }

    @Override
    public Bullet shoot(Vec2 position) {
        return super.shoot(new FireBullet(position, this.getCategory(), this.getMask()));
    }
}