package fr.upemlv.escapeira.model.element;

import java.util.ArrayList;
import java.util.Random;

import org.jbox2d.common.Vec2;

import android.graphics.PointF;

import fr.upemlv.escapeira.R;
import fr.upemlv.escapeira.level.AbstractLevel;
import fr.upemlv.escapeira.model.weapon.*;


/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */

public class BossShip extends EnnemyShip {
    public BossShip(int life, String weapon, ArrayList<PointF> points) {
        super(life, MissileWeapon.ID, R.drawable.bossship, points);
    }

    public void update() {
        this.changeWeapon(EnemyShipFactory.WEAPONS[new Random().nextInt(EnemyShipFactory.WEAPONS.length)]);
    }

    @Override
    public Bullet shoot() {
        Vec2 vecPos = this.getBody().getPosition().clone();
        vecPos.y = vecPos.y - 5;

        Bullet bullet = this.getSelectedWeapon().shoot(vecPos);
        if (bullet != null)
            AbstractLevel.getBullets().add(bullet);

        return bullet;
    }
}