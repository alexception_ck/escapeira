package fr.upemlv.escapeira.model.element;

import java.util.ArrayList;
import java.util.Random;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.joints.MouseJoint;
import org.jbox2d.dynamics.joints.MouseJointDef;

import android.graphics.PointF;

import fr.upemlv.escapeira.gesture.GestureRecognizer;
import fr.upemlv.escapeira.level.AbstractLevel;
import fr.upemlv.escapeira.physic.Category;
import fr.upemlv.escapeira.physic.World;


/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public abstract class EnnemyShip extends AbstractShip {
    private static final int DELAY = 20;
    private int delayed = EnnemyShip.DELAY;
	private MouseJointDef md;
	private MouseJoint m;
	private ArrayList<PointF> points;
	
    public EnnemyShip(int life, String weapon, int idDrawable, ArrayList<PointF> points) {
        super(World.toWorld(points.get(0)), life, weapon, Category.ENNEMY.getValue(), Category.PLAYER_BULLET.getValue()|Category.WALL.getValue(), idDrawable);
        this.getBody().setUserData(this);
		md = new MouseJointDef();
    	BodyDef bd = new BodyDef();
    	bd.position = super.getBody().getPosition();
    	Body b = new Body(bd, World.WORLD);
    	md.bodyA = b;
    	md.bodyB = super.getBody();
    	md.target.set(bd.position);
    	md.maxForce = 1;
    	m = (MouseJoint) World.WORLD.createJoint(md);
    	this.points = points;
    }

    public abstract Bullet shoot();

    protected Bullet shoot(Bullet bullet) {
        Vec2 vecPos = this.getBody().getPosition().clone();
        vecPos.y = vecPos.y - 5;
        AbstractLevel.getBullets().add(bullet);
        return bullet;
    }
    public void move(ArrayList<PointF> points) {
    	super.getBody().setAwake(true);
    	for (PointF pointF : points) {
    		m.setTarget(World.toWorld(pointF));
		}
    	
    }
    public void update() {
        if (this.delayed > 0) {
            this.delayed--;
            return;
        }
        this.move(this.points);
        Random randomer = new Random();
        switch (GestureRecognizer.GestureID.values()[randomer.nextInt(4)]) {
        case SHOOT:
        	int x = randomer.nextInt(2);
        	Bullet bullet = this.shoot();
        	if (bullet != null)
        		bullet.move(new Vec2(x, -randomer.nextInt(10)), 100);
        	break;
//            case ATTACK:
//                this.move(new Vec2(0, -1), randomer.nextInt(10)+1);
//            case MOVE:
//                int x = randomer.nextInt(2);
//                x = (randomer.nextInt(2) == 1) ? x : -x;
//                Bullet bullet = this.shoot();
//                if (bullet != null)
//                    bullet.move(new Vec2(x, -randomer.nextInt(10)), 100);
//                this.shiboleetHack(new Vec2(x, -randomer.nextInt(10)));
//                break;
//            case LEFT_DRIFT:
//                this.move(new Vec2(-10, 5), randomer.nextInt(10)+1);
//                break;
//            case RIGHT_DRIFT:
//                this.move(new Vec2(10, 5), randomer.nextInt(10)+1);
//                break;
            default:
//                this.move(new Vec2(0, -1));
                break;
        }
        this.move();
        this.delayed = EnnemyShip.DELAY;
    }

	public void updatePos(int getyScroll) {
		Vec2 pos = this.getBody().getPosition();
		PointF posPoint = World.toPixel(pos);
		posPoint.y = posPoint.y + getyScroll;
		pos = new Vec2(World.toWorld(posPoint));
		this.getBody().setTransform(pos, this.getBody().getAngle());
	}
}