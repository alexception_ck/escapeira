package fr.upemlv.escapeira.model.element;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.joints.MouseJoint;
import org.jbox2d.dynamics.joints.MouseJointDef;

import fr.upemlv.escapeira.EscapeIR;
import fr.upemlv.escapeira.GameActivity;
import fr.upemlv.escapeira.R;
import fr.upemlv.escapeira.gesture.GestureRecognizer;
import fr.upemlv.escapeira.level.AbstractLevel;
import fr.upemlv.escapeira.model.weapon.MissileWeapon;
import fr.upemlv.escapeira.physic.Category;
import fr.upemlv.escapeira.physic.World;
import android.graphics.PointF;
import android.util.Log;
import android.view.View;

public class PlayerShip extends AbstractShip {
	private static Vec2 INITIAL_POSITION = new Vec2(World.toWorld(new PointF((int) (EscapeIR.getWidth() / 2), (int) (EscapeIR.getHeight() * 9 / 10))));
	private MouseJointDef md;
	private MouseJoint m;
	
	public PlayerShip(int life) {
		super(PlayerShip.INITIAL_POSITION, life, MissileWeapon.ID, Category.PLAYER.getValue(),
				Category.WALL.getValue()|Category.ENNEMY_BULLET.getValue()|Category.ENNEMY.getValue()|Category.BOSS.getValue(), R.drawable.playership);
		this.getBody().setUserData(this);
		md = new MouseJointDef();
    	BodyDef bd = new BodyDef();
    	bd.position = super.getBody().getPosition();
    	Body b = new Body(bd, World.WORLD);
    	md.bodyA = b;
    	md.bodyB = super.getBody();
    	md.target.set(bd.position);
    	md.maxForce = 1;
    	m = (MouseJoint) World.WORLD.createJoint(md);
	}

	@Override
	public Bullet shoot() {
		Vec2 vecPos = this.getBody().getPosition().clone();
		vecPos.y = vecPos.y +5;

		Bullet bullet = this.getSelectedWeapon().shoot(vecPos);
		if (bullet != null)
			AbstractLevel.getBullets().add(bullet);
		return bullet;
	}

	public void isHit(){
		if(this.isAlive()){
			this.setLife(this.getLife() - 1);
//			Menu.setLife(Menu.getLife() - 1);
		}
	}
	/**
	 * Detect on which weapon the user just clicked
	 * 
	 */
	 public void selectedWeapon(){
//		 if (PointMath.distance(GestureRecognizer.getInstance().getStartingPoint(), new PointF(140, 10)) <= 10) {
//			this.changeWeapon(MissileWeapon.ID);
//		}
//		if(GestureRecognizer.getInstance().getStartingPoint().distance(new Point(200, 10)) <= 10){
//			this.changeWeapon(FireWeapon.ID);
//		}
//		if(GestureRecognizer.getInstance().getStartingPoint().distance(new Point(270, 10)) <= 10){
//			this.changeWeapon(ShiboleetWeapon.ID);
//		}
	 }

	 @Override
	public void move(Vec2 impulse, PointF ending) {
		 	ending = GestureRecognizer.getInstance().getLast();
	    	if (ending == null) {
				return;
			}	    	
	    	super.getBody().setAwake(true);
	    	m.setTarget(World.toWorld(ending));
	}
	 
	 @Override
	 public void update() {
//		 Menu.setLife(this.getLife());
//		 Menu.setMissile(this.getWeapons().get(MissileWeapon.ID).getAmmo());
//		 Menu.setFire(this.getWeapons().get(FireWeapon.ID).getAmmo());
//		 Menu.setShiboolet(this.getWeapons().get(ShiboleetWeapon.ID).getAmmo());

		 Vec2 direction = GestureRecognizer.getInstance().getAngle();
		 if(!GestureRecognizer.getInstance().getPoints().isEmpty()){
			 if(super.getBody().getPosition() == World.toWorld(GestureRecognizer.getInstance().getStartingPoint()));
		 		this.move(direction);
		 }
		 switch (GestureRecognizer.getInstance().getGesture()) {
		 case SHOOT:
			 Log.w("GESTURE", "SHOOT");
			 Bullet bulletShoot = this.shoot();
			 if(bulletShoot != null){
				 bulletShoot.move(direction, 100);
				 GestureRecognizer.getInstance().clearGesture();
			 }
			 break;
//		 case MOVE:
//			 this.move(direction);
//		 case SHOOT:
//			 System.out.println(direction);
//			 Bullet bulletShoot = this.shoot();
//			 if (bulletShoot != null)
//				 bulletShoot.move(direction, 100);
//			 this.shiboleetHack(direction);
//			 break;
//		 case LEFT_DRIFT:
//			 this.move(direction);
//			 break;20
//		 case RIGHT_DRIFT:
//			 this.move(direction);
//			 break;
//		 case SELECT:
//			 this.selectedWeapon();
//			 break;
//		 case LEFT_LOOPING:
//			 this.setRunningGesture(GestureID.LEFT_LOOPING);
//			 this.move(new Vec2(-1, 0), 10);
//			 break;
//		 case RIGHT_LOOPING:
//			 this.setRunningGesture(GestureID.RIGHT_LOOPING);
//			 this.move(new Vec2(1, 0), 10);
//			 break;
		 default:
			 break;
		 }
	 }
}