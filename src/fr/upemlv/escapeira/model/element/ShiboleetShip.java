package fr.upemlv.escapeira.model.element;

import java.util.ArrayList;

import android.graphics.PointF;

import fr.upemlv.escapeira.R;
import fr.upemlv.escapeira.model.weapon.ShiboleetWeapon;
import fr.upemlv.escapeira.physic.Category;


/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class ShiboleetShip extends EnnemyShip {
    public ShiboleetShip(ArrayList<PointF> points) {
        super(20, ShiboleetWeapon.ID, R.drawable.shiboleetship, points);
    }

    @Override
    public Bullet shoot() {
        return super.shoot(new ShiboleetBullet(this.getBody().getPosition().clone(), Category.ENNEMY_BULLET.getValue(), Category.PLAYER.getValue()));
    }
}