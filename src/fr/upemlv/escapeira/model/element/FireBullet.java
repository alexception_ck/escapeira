package fr.upemlv.escapeira.model.element;

import org.jbox2d.common.Vec2;

import fr.upemlv.escapeira.physic.World;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;

/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class FireBullet extends Bullet {
    public FireBullet(Vec2 position, int category, int mask) {
        super(10, position, category, mask);
    }

	public void render(Canvas canvas) {
        PointF position = World.toPixel(this.getBody().getPosition());
        Paint paint = new Paint();
        paint.setColor(Color.YELLOW);
        canvas.drawCircle(position.x, position.y, 10, paint);
	}
}