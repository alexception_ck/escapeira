package fr.upemlv.escapeira.model.element;

import java.util.ArrayList;

import android.graphics.PointF;

import fr.upemlv.escapeira.R;
import fr.upemlv.escapeira.model.weapon.MissileWeapon;
import fr.upemlv.escapeira.physic.Category;

/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */public class MissileShip extends EnnemyShip {
    public MissileShip(ArrayList<PointF> points) {
        super(5, MissileWeapon.ID, R.drawable.missileship, points);
    }

    @Override
    public Bullet shoot() {
        return super.shoot(new MissileBullet(this.getBody().getPosition().clone(), Category.ENNEMY_BULLET.getValue(), Category.PLAYER.getValue()));
    }

	public void update() {
		// TODO Auto-generated method stub
		
	}
}