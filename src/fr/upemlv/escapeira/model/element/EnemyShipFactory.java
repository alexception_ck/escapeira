package fr.upemlv.escapeira.model.element;

import java.util.ArrayList;
import java.util.Random;

import android.graphics.PointF;

import fr.upemlv.escapeira.model.weapon.FireWeapon;
import fr.upemlv.escapeira.model.weapon.MissileWeapon;
import fr.upemlv.escapeira.model.weapon.ShiboleetWeapon;


/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public abstract class EnemyShipFactory {
    /**
     * 
     * @author Joachim ARCHAMBAULT, Alexandre ANDRE
     *
     */
    public enum Difficulty {
        EASY,
        MEDIUM,
        HARD,
    };

    public static String[] WEAPONS = {MissileWeapon.ID, FireWeapon.ID, ShiboleetWeapon.ID};

    private EnemyShipFactory() {
        throw null;
    }

    public static EnnemyShip buildRandomShip(int life) {
    	//1 fire
    	//2 shiboleet
    	//3 missile
        String ships[] = {"fire", "shiboleet", "missile"};
        Random randomer = new Random();
        EnnemyShip ship;
        
        switch (randomer.nextInt(3)) {
            case 0:
            	ArrayList<PointF> points = new ArrayList<PointF>();
            	for (int i = 0; i < 100; i++) {
            		points.add(new PointF(randomer.nextFloat()*20, randomer.nextFloat()*20));
        		}
                ship = new FireShip(points);
                break;
            case 1:
            	ArrayList<PointF> points2= new ArrayList<PointF>();
            	for (int i = 0; i < 100; i++) {
            		points2.add(new PointF(randomer.nextFloat()*20, randomer.nextFloat()*20));
        		}
                ship = new ShiboleetShip(points2);
                break;
            default:
            	ArrayList<PointF> points3= new ArrayList<PointF>();
            	for (int i = 0; i < 100; i++) {
            		points3.add(new PointF(randomer.nextFloat()*20, randomer.nextFloat()*20));
        		}
                ship = new MissileShip(points3);
                break;
        }
        return ship;
    }

    public static ArrayList<EnnemyShip> buildShipArmy(int shipsNumber) {
        ArrayList<EnnemyShip> ships = new ArrayList<EnnemyShip>();
        Random randomer = new Random();
        int life = 10;
        for (int i = 0 ; i < randomer.nextInt(shipsNumber+1) ; i++)
            ships.add(EnemyShipFactory.buildRandomShip(life));
        
        return ships;
}
}