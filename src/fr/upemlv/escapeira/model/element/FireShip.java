package fr.upemlv.escapeira.model.element;

import java.util.ArrayList;

import android.graphics.PointF;

import fr.upemlv.escapeira.R;
import fr.upemlv.escapeira.model.weapon.FireWeapon;
import fr.upemlv.escapeira.physic.Category;


/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class FireShip extends EnnemyShip {
    public FireShip(ArrayList<PointF> points) {
        super(10, FireWeapon.ID, R.drawable.fireship, points);
    }

    @Override
    public Bullet shoot() {
        return super.shoot(new FireBullet(this.getBody().getPosition().clone(), Category.ENNEMY_BULLET.getValue(), Category.PLAYER.getValue()));
    }

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
}