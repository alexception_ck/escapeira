package fr.upemlv.escapeira.model.element;

import org.jbox2d.common.Vec2;

import fr.upemlv.escapeira.physic.World;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;

/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public class ShiboleetBullet extends Bullet {
    public ShiboleetBullet(Vec2 position, int category, int mask) {
        super(5, position, category, mask);
    }
/*
    @Override
    public void render(Graphics2D graphics) {
    }
*/

	public void render(Canvas canvas) {
        PointF position = World.toPixel(this.getBody().getPosition());
        Paint paint = new Paint();
        paint.setColor(Color.GRAY);
        canvas.drawCircle(position.x, position.y, 10, paint);
//        graphics.setColor(Color.GRAY);
//        graphics.fill(new Ellipse2D.Float((float) position.getX(), (float) position.getY(), 10, 10));
	}
}