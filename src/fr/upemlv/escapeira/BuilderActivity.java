package fr.upemlv.escapeira;

import java.util.ArrayList;

import fr.upemlv.escapeira.gesture.GestureRecognizer;
import fr.upemlv.escapeira.model.weapon.FireWeapon;
import fr.upemlv.escapeira.model.weapon.MissileWeapon;
import fr.upemlv.escapeira.model.weapon.ShiboleetWeapon;
import fr.upemlv.escapeira.utils.DrawablesHolder;
import fr.upemlv.escapeira.view.BuilderView;
import fr.upemlv.escapeira.view.CaptureView;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class BuilderActivity extends Activity {
	private BuilderView builderView;
	private AlertDialog.Builder shipsBuilder;
	private AlertDialog ships;
	private AlertDialog.Builder moveBuilder;
	private AlertDialog move;
	ArrayList<PointF> points = new ArrayList<PointF>();
	ArrayList<PointF> position = new ArrayList<PointF>();
	private Handler handlerShip = new Handler() {
		public void handleMessage(android.os.Message msg) {
			ships.show();
		}
	};
	private Handler handlerMove = new Handler() {
		public void handleMessage(android.os.Message msg) {
			move.show();
		}
	};
	private Handler handlerCloseMove = new Handler() {
		public void handleMessage(android.os.Message msg) {
			move.dismiss();
			points = GestureRecognizer.getInstance().getPoints();
			ArrayList<PointF> tmp = new ArrayList<PointF>();
			float varX = points.get(0).x - position.get(0).x;
			float varY = points.get(0).y - position.get(0).y;
			tmp.add(position.get(0));
			for (int i = 1; i < points.size(); i++) {
				tmp.add(new PointF(points.get(i).x + varX, points.get(i).y + varY));
			}
			GestureRecognizer.getInstance().clearGesture();
			builderView.getLevel().addShiboleetShip(tmp);
			points.clear();
		}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent intent = this.getIntent();
		this.builderView = new BuilderView(this, intent.getStringExtra("name"), intent.getIntExtra("duration", 180), handlerShip);
		setContentView(this.builderView);
	}
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		shipsBuilder = new AlertDialog.Builder(this);
		moveBuilder = new AlertDialog.Builder(this);
		
		ImageButton fireShip = new ImageButton(this);
		fireShip.setImageBitmap(DrawablesHolder.getInstance().getImage(R.drawable.fireship));
		fireShip.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				ships.dismiss();
			}
		});
		
		ImageButton missileShip = new ImageButton(this);
		missileShip.setImageBitmap(DrawablesHolder.getInstance().getImage(R.drawable.missileship));
		missileShip.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				ships.dismiss();
			}
		});
		
		ImageButton ShiboleetShip = new ImageButton(this);
		ShiboleetShip.setImageBitmap(DrawablesHolder.getInstance().getImage(R.drawable.shiboleetship));
		ShiboleetShip.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("unchecked")
			public void onClick(View v) {
				position = (ArrayList<PointF>) GestureRecognizer.getInstance().getPoints().clone();
				GestureRecognizer.getInstance().clearGesture();
				handlerMove.sendEmptyMessage(0);
				ships.dismiss();
			}
		});
		LinearLayout shipButtons = new LinearLayout(this);
		shipButtons.addView(fireShip);
		shipButtons.addView(missileShip);
		shipButtons.addView(ShiboleetShip);
		
		CaptureView moveView = new CaptureView(this, handlerCloseMove);
		moveView.setMinimumHeight(EscapeIR.getHeight());
		moveView.setMinimumWidth(EscapeIR.getWidth());
		moveView.setBackgroundColor(Color.GRAY);
		
		this.shipsBuilder.setTitle("Choose an enemy : ");
		this.shipsBuilder.setView(shipButtons);
		
		this.moveBuilder.setTitle("Make the path of the ship");
		this.moveBuilder.setView(moveView);
		
		this.move = this.moveBuilder.create();
		this.ships = this.shipsBuilder.create();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.builder, menu);
		return true;
	}

}
