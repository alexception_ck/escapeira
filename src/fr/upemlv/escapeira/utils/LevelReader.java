package fr.upemlv.escapeira.utils;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LevelReader {
	private Document doc;

	public LevelReader(String fileName) throws ParserConfigurationException, SAXException, IOException {
		File levelFile = new File(fileName);
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		this.doc = docBuilder.parse(levelFile);
		//Seems to be important
		this.doc.getDocumentElement().normalize();

		NodeList enemies = doc.getElementsByTagName("enemy");
	}
}
