package fr.upemlv.escapeira.utils;

import android.graphics.Canvas;

/**
 * Define an interface for Renderable objects (i.e. drawable objects)
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public interface Renderable {
    /**
     * @param graphics the graphics manager
     */
    public void render(Canvas canvas);
}