package fr.upemlv.escapeira.utils;

import java.util.concurrent.ConcurrentHashMap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import fr.upemlv.escapeira.EscapeIR;



public class DrawablesHolder {
	private static DrawablesHolder instance = null;
	private final ConcurrentHashMap<Integer, Bitmap> drawables;

	private DrawablesHolder() {
			this.drawables = new ConcurrentHashMap<Integer, Bitmap>();
	}

	public static DrawablesHolder getInstance() {
		if (DrawablesHolder.instance == null)
			DrawablesHolder.instance = new DrawablesHolder();

		return DrawablesHolder.instance;
	}

	public Bitmap getImage(int id) {
		synchronized (this.drawables) {
			if (this.drawables.get(id) == null)
				this.drawables.put(id, BitmapFactory.decodeResource(EscapeIR.getContext().getResources(), id));
			return this.drawables.get(id);
		}
	}
}
