package fr.upemlv.escapeira.utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.graphics.Point;
import android.graphics.PointF;


public class LevelWriter {
	private Document doc;
	private Element enemies;

	public LevelWriter(String name, int duration) throws ParserConfigurationException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		this.doc = docBuilder.newDocument();
		this.doc.appendChild(this.doc.createElement("level"));

		//name
		Element nameElement = this.doc.createElement("name");
		nameElement.appendChild(this.doc.createTextNode(name));
		this.doc.appendChild(nameElement);

		//duration
		Element durationElement = this.doc.createElement("duration");
		durationElement.appendChild(this.doc.createTextNode("" + duration));
		this.doc.appendChild(durationElement);
		
		//enemies
		this.enemies = this.doc.createElement("enemies");
		this.doc.appendChild(this.enemies);
	}

	public void addEnemy(String type, Point position, ArrayList<PointF> motion) {
		Element enemy = this.doc.createElement("enemy");
		
		Element typeElement = this.doc.createElement("type");
		typeElement.appendChild(this.doc.createTextNode(type));
		enemy.appendChild(typeElement);

		Element positionElement = this.doc.createElement("position");
		Attr x = this.doc.createAttribute("x");
		x.setValue("" + position.x);
		positionElement.setAttributeNode(x);
		Attr y = this.doc.createAttribute("y");
		y.setValue("" + position.y);
		positionElement.setAttributeNode(y);
		
		enemy.appendChild(positionElement);

		Element motions = this.doc.createElement("motions");
		for (PointF pointVert : motion) {
			Element motionElement = this.doc.createElement("motion");
			Attr motionX = this.doc.createAttribute("x");
			motionX.setValue("" + pointVert.x);
			motionElement.setAttributeNode(motionX);
			Attr motionY = this.doc.createAttribute("y");
			motionY.setValue("" + pointVert.y);
			motionElement.setAttributeNode(motionY);

			motions.appendChild(motionElement);
		}

		enemy.appendChild(motions);

		this.enemies.appendChild(enemy);
	}
}
