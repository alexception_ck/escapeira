package fr.upemlv.escapeira.utils;

public class FPSManager {
	private final long fps;
	private long currentTime;
	private long previousTime;

	public FPSManager(long fps) {
		this.fps = 1 / fps;
		this.updateCurrentTime();
		this.previousTime = this.currentTime;
	}

	public void updateCurrentTime() {
		this.currentTime = System.currentTimeMillis();
	}

	public void updatePreviousTime() {
		this.previousTime = System.currentTimeMillis();
	}

	public long getDelta() {
		return this.currentTime - this.previousTime;
	}

	public long sleepFor() {
		return (this.fps - this.getDelta() > 0) ?
				this.fps - this.getDelta()
				: 0;
	}
}
