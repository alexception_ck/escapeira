package fr.upemlv.escapeira.utils;

import android.graphics.PointF;

public class PointMath {

	public static double distance(PointF point1, PointF point2) {
		double distance = Math.sqrt(PointMath.sq(point2.x - point1.x) + PointMath.sq(point2.y - point1.y));
		return distance;
	}
	
    static public float sq(float a) {
        return a*a;
    }
    
    public static double ptLineDist(PointF point1, PointF point2, PointF point3)
    		    {
    		      double pd2 = sq(point1.x - point2.x) + sq(point1.y - point2.y);
    		  
    		      double x, y;
    		      if (pd2 == 0)
    		        {
    		          // Points are coincident.
    		          x = point1.x;
    		          y = point2.y;
    		        }
    		      else
    		        {
    		          double u = ((point3.x - point1.x) * (point2.x - point1.x) + (point3.y - point1.y) * (point2.y - point1.y)) / pd2;
    		          x = point1.x + u * (point2.x - point1.x);
    		          y = point1.y + u * (point2.y - point1.y);
    		        }
    		  
    		      return Math.sqrt((x - point3.x) * (x - point3.x) + (y - point3.y) * (y - point3.y));
    		    }
	
}
