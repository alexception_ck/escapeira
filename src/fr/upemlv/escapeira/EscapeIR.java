package fr.upemlv.escapeira;

import android.app.Application;
import android.content.Context;
import android.view.Display;
import android.view.WindowManager;

public class EscapeIR extends Application {
	private static Context context;

	public void onCreate() {
		super.onCreate();
		EscapeIR.context = getApplicationContext();
	}

	public static Context getContext() {
		return EscapeIR.context;

	}

	protected static Display getDisplay() {
		return ((WindowManager) EscapeIR.getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
	}
	
	public static int getWidth() {
		return EscapeIR.getDisplay().getWidth();
	}

	public static int getHeight() {
		return EscapeIR.getDisplay().getHeight();
	}
	public static int getDensity(){
		return EscapeIR.getContext().getResources().getDisplayMetrics().densityDpi;
	}
}
