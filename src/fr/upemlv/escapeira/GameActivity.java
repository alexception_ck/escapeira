package fr.upemlv.escapeira;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import fr.upemlv.escapeira.model.weapon.FireWeapon;
import fr.upemlv.escapeira.model.weapon.MissileWeapon;
import fr.upemlv.escapeira.model.weapon.ShiboleetWeapon;
import fr.upemlv.escapeira.utils.PointMath;
import fr.upemlv.escapeira.view.GameView;

public class GameActivity extends Activity {
	private GameView gameView;
	private FrameLayout gameLayout;
	private LinearLayout gameButtons;
	public static double diagonale;
	TextView missileAmmo;
	TextView fireAmmo;
	TextView shiboleetAmmo;

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			missileAmmo.setText(gameView.getLevel().getPlayer().getWeapons().get(MissileWeapon.ID).getAmmo().toString());
			fireAmmo.setText(gameView.getLevel().getPlayer().getWeapons().get(FireWeapon.ID).getAmmo().toString());
			shiboleetAmmo.setText(gameView.getLevel().getPlayer().getWeapons().get(ShiboleetWeapon.ID).getAmmo().toString());
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.gameView = new GameView(this,handler);
		this.gameLayout = new FrameLayout(this);
		this.gameButtons = new LinearLayout(this);
		
		
		
		Button fire = new Button(this);
		fire.setText("fire");
		fire.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				gameView.getLevel().getPlayer().changeWeapon(FireWeapon.ID);
			}
		});
		
		fireAmmo = new TextView(this);
		fireAmmo.setBackgroundColor(Color.BLACK);
		fireAmmo.setTextColor(Color.WHITE);
		
		Button missile = new Button(this);
		missile.setText("missile");
		missile.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				gameView.getLevel().getPlayer().changeWeapon(MissileWeapon.ID);
			}
		});

		missileAmmo = new TextView(this);
		missileAmmo.setBackgroundColor(Color.BLACK);
		missileAmmo.setTextColor(Color.WHITE);
		
		Button shiboleet = new Button(this);
		shiboleet.setText("shiboleet");
		shiboleet.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				gameView.getLevel().getPlayer().changeWeapon(ShiboleetWeapon.ID);
			}
		});
		
		shiboleetAmmo = new TextView(this);
		shiboleetAmmo.setBackgroundColor(Color.BLACK);
		shiboleetAmmo.setTextColor(Color.WHITE);
		
		gameButtons.addView(missile);  
		gameButtons.addView(missileAmmo);
		gameButtons.addView(fire);
		gameButtons.addView(fireAmmo);
		gameButtons.addView(shiboleet);
		gameButtons.addView(shiboleetAmmo);
		
		diagonale = Math.sqrt(PointMath.sq(EscapeIR.getWidth()) + PointMath.sq(EscapeIR.getHeight()));
		gameLayout.addView(gameView);  
		gameLayout.addView(gameButtons); 
		setContentView(this.gameLayout);
	}
}
