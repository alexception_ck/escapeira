package fr.upemlv.escapeira;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MenuActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);

		final Button play = (Button) this.findViewById(R.id.menu_play);
		final Button build = (Button) this.findViewById(R.id.menu_builder);
		play.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent playIntent = new Intent(v.getContext(), GameActivity.class);
				startActivityForResult(playIntent, 0);
			}
		});
		build.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent playIntent = new Intent(v.getContext(), PreBuilderActivity.class);
				startActivityForResult(playIntent, 0);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

}