package fr.upemlv.escapeira.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View.OnTouchListener;
import fr.upemlv.escapeira.utils.FPSManager;

public abstract class AbstractSurfaceView extends SurfaceView implements SurfaceHolder.Callback{
	class GameThread extends Thread {
		private SurfaceHolder holder;
		private AbstractSurfaceView view;
		private boolean run;
		private FPSManager fps = new FPSManager(60);

		//OK
		GameThread(SurfaceHolder holder, AbstractSurfaceView view) {
			super();
			this.holder = holder;
			this.view	= view;
		}

		//OK
		public void setRunning(boolean run) {
			this.run = run;
		}

		public SurfaceHolder getHolder() {
			return this.holder;
		}

		public void run() {
			Canvas c;

			while (this.run) {
				c = null;
				this.view.update();
				this.fpsDelayer();
				this.draw(c);
			}
		}

		private void fpsDelayer() {
			fps.updateCurrentTime();
			try {
				Thread.sleep(this.fps.sleepFor());
			} catch (InterruptedException e) {
			}
			fps.updatePreviousTime();
		}

		private void draw(Canvas c) {
			try {
				c = this.holder.lockCanvas(null);
				synchronized (this.holder) {
					c.drawColor(Color.BLACK);
					this.view.onDraw(c);
				}
			} finally {
				if (c != null) {
					this.holder.unlockCanvasAndPost(c);
				}
			}
		}
	}

	private GameThread thread;

	//OK
	public AbstractSurfaceView(Context context) {
		super(context);
		this.getHolder().addCallback(this);
		setFocusable(true);
	}

	public final void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	}

	//OK
	public final void surfaceCreated(SurfaceHolder holder) {
		this.thread = new GameThread(holder, this);
		this.thread.setRunning(true);
		this.thread.start();
	}

	//OK
	public final void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        thread.setRunning(false);

        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
	}

	abstract protected void update();

	@Override
	abstract protected void onDraw(Canvas canvas);

}
