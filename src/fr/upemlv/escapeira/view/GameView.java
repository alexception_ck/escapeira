package fr.upemlv.escapeira.view;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.view.MotionEvent;
import fr.upemlv.escapeira.gesture.GestureRecognizer;
import fr.upemlv.escapeira.level.AbstractLevel;
import fr.upemlv.escapeira.level.Earth;

public class GameView extends AbstractSurfaceView {
	private static final String TAG = "GameView";
	private final Handler handler;

	private AbstractLevel level;

	public GameView(Context context, Handler handler) {
		super(context);
		this.handler = handler;
		this.level = new Earth();
	}
	
	@Override
	protected void update() {
		this.handler.sendEmptyMessage(0);
		if (this.level.getState() == 1)
			this.level.update();
	}
	@Override
	protected void onDraw(Canvas canvas) {
		this.level.render(canvas);
		GestureRecognizer.getInstance().render(canvas);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			GestureRecognizer.getInstance().down();
			return true;
		case MotionEvent.ACTION_MOVE:
			GestureRecognizer.getInstance().update(event.getX(), event.getY());
			return true;
		case MotionEvent.ACTION_UP:
			GestureRecognizer.getInstance().analyze();
			return true;
		default:
			return false;
		}
	}
	
	public AbstractLevel getLevel() {
		return level;
	}

}
