package fr.upemlv.escapeira.view;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import fr.upemlv.escapeira.gesture.GestureRecognizer;
import fr.upemlv.escapeira.level.Level;

public class BuilderView extends AbstractSurfaceView {
	private static final String TAG = "BuilderView";
	private String name;
	private int duration;
	private final Level level;

	public BuilderView(Context context, String name, int duration, Handler handler) {
		super(context);
		this.level = new Level(this, name, duration, handler);
	}

	@Override
	protected void update() {
		this.level.update();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		this.level.render(canvas);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				GestureRecognizer.getInstance().down();
				return true;
			case MotionEvent.ACTION_MOVE:
				GestureRecognizer.getInstance().update(event.getX(), event.getY());
				return true;
			case MotionEvent.ACTION_UP:
				GestureRecognizer.getInstance().analyze();
				return true;
			default:
				return false;
		}
	}
	public Level getLevel() {
		return level;
	}
}
