package fr.upemlv.escapeira;

import java.util.HashMap;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class PreBuilderActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pre_builder);

		Spinner durationMinutes = (Spinner) this.findViewById(R.id.duration_input_min);
		durationMinutes.setAdapter(this.populateSpinner(R.array.duration_minutes));
		Spinner durationSeconds = (Spinner) this.findViewById(R.id.duration_input_sec);
		durationSeconds.setAdapter(this.populateSpinner(R.array.duration_seconds));

		Button cancel 	= (Button) this.findViewById(R.id.validation_cancel);
		Button build	= (Button) this.findViewById(R.id.validation_build);

		cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				switchView(v, MenuActivity.class);
			}
		});

		build.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				EditText levelName = (EditText) findViewById(R.id.level_name_input);
				Spinner durationMinutes = (Spinner) findViewById(R.id.duration_input_min);
				Spinner durationSeconds = (Spinner) findViewById(R.id.duration_input_sec);
				if (levelName.getText().length() == 0) {
					levelName.setError(getText(R.string.prebuilder_level_name_empty));
				} else {
					Intent intent = new Intent(v.getContext(), BuilderActivity.class);
					intent.putExtra("name", levelName.getText());
					intent.putExtra("duration", (((durationMinutes.getSelectedItemId() + 1) * 60) + (durationSeconds.getSelectedItemId() * 15)));
					startActivityForResult(intent, 0);
				}
			}
		});
	}

	protected void switchView(View view, Class<?> activity) {
		Intent intent = new Intent(view.getContext(), activity);
		startActivityForResult(intent, 0);
	}

	private ArrayAdapter<CharSequence> populateSpinner(int listId) {
		ArrayAdapter<CharSequence> spinnerContent = ArrayAdapter.createFromResource(this, listId, android.R.layout.simple_spinner_item);
		spinnerContent.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		return spinnerContent;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pre_builder, menu);
		return true;
	}

}
