package fr.upemlv.escapeira.level;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import fr.upemlv.escapeira.EscapeIR;
import fr.upemlv.escapeira.R;
import fr.upemlv.escapeira.gesture.Gesture.Orientation;
import fr.upemlv.escapeira.gesture.GestureRecognizer;
import fr.upemlv.escapeira.gesture.TranslationGesture;
import fr.upemlv.escapeira.model.element.EnnemyShip;
import fr.upemlv.escapeira.model.element.FireShip;
import fr.upemlv.escapeira.model.element.MissileShip;
import fr.upemlv.escapeira.model.element.ShiboleetShip;
import fr.upemlv.escapeira.utils.DrawablesHolder;
import fr.upemlv.escapeira.utils.Renderable;
import fr.upemlv.escapeira.utils.Updatable;
import fr.upemlv.escapeira.view.AbstractSurfaceView;

public class Level implements Updatable, Renderable {
	protected class Background implements Updatable, Renderable {
		private Bitmap backgroundImage;
		private int yScroll = 0;
		private int height;
		private View view;
		private Handler handler;
		public Background(int height, View v, Handler handler) {
			this.height = height;
			this.backgroundImage	= DrawablesHolder.getInstance().getImage(R.drawable.gamebg);
			this.handler = handler;
			this.view = v;
		}

		public void update() {
	 		switch (GestureRecognizer.getInstance().getGesture()) {
			 	case SHOOT:
			 		if (GestureRecognizer.getInstance().getGestureObject() == null
			 			|| GestureRecognizer.getInstance().getPoints().size() == 0)
			 			break;
			 		Orientation orientation = ((TranslationGesture) GestureRecognizer.getInstance().getGestureObject()).getOrientation();
			 		if (orientation == Orientation.NORTH
			 				|| orientation == Orientation.NORTH_WEST
			 				|| orientation == Orientation.NORTH_EAST) {
						this.yScroll += P_PER_S;// * delta;
						if (this.yScroll > EscapeIR.getHeight())
							this.yScroll = EscapeIR.getHeight();
			 		}
			 		else if (orientation == Orientation.SOUTH
			 				|| orientation == Orientation.SOUTH_WEST
			 				|| orientation == Orientation.SOUTH_EAST) {
						this.yScroll -= P_PER_S;// * delta;
						if (this.yScroll < 0)
							this.yScroll = 0;
			 		}
			 		GestureRecognizer.getInstance().getPoints().remove(0);
//			 		GestureRecognizer.getInstance().clearGesture();
			 		break;

			 	case SELECT:
			 		Log.w("CLICK", "CLICKED");
					this.handler.sendEmptyMessage(0);
			 		break;
			 	default:
			 		break;
	 		}
		}


		public void render(Canvas canvas) {

			WindowManager wm = (WindowManager) EscapeIR.getContext().getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();

			for (int x = 0 ; x <= display.getWidth() ; x += this.backgroundImage.getWidth()) {
				for (int y = -this.backgroundImage.getHeight() ; y <= display.getHeight() ; y += this.backgroundImage.getHeight()) {
					canvas.drawBitmap(this.backgroundImage, x, y + yScroll , null);
				}
			}
		}
		public int getyScroll() {
			return yScroll;
		}
	}

	//PIXELS PER SECOND
	public static final int P_PER_S = 10;
	
	private final AbstractSurfaceView view;
	private final Background background;
	private ArrayList<EnnemyShip> currentShips;
	
	public Level(AbstractSurfaceView view, String name, int duration, Handler handler) {
		super();
		this.view = view;
		this.background = new Background(duration * Level.P_PER_S, view, handler);
		this.currentShips = new ArrayList<EnnemyShip>();
	}
	
	public void addFireShip(ArrayList<PointF> move) {
		FireShip ship = new FireShip(move);
		this.currentShips.add(ship);
	}
	public void addMissileShip(ArrayList<PointF> move) {
		MissileShip ship = new MissileShip(move);
		this.currentShips.add(ship);
	}
	public void addShiboleetShip(ArrayList<PointF> move) {
		ShiboleetShip ship = new ShiboleetShip(move);
		this.currentShips.add(ship);
	}
	public void update() {
		this.background.update();
		
		if(!currentShips.isEmpty()){
			for (EnnemyShip ship : currentShips) {
				ship.updatePos(this.background.getyScroll());
				ship.update();
			}
		}
		this.background.update();
	}
	@Override
	public void render(Canvas canvas) {

		this.background.render(canvas);
		if(!currentShips.isEmpty()){
			for (EnnemyShip ship : currentShips) {
				ship.render(canvas);
			}
		}
		GestureRecognizer.getInstance().render(canvas);
	}

	public Background getBackground() {
		return this.background;
	}
}
