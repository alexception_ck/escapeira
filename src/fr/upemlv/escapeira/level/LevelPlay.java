package fr.upemlv.escapeira.level;

import java.util.ArrayList;
import java.util.Random;

import org.jbox2d.common.Vec2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import fr.upemlv.escapeira.model.element.AbstractElement;
import fr.upemlv.escapeira.model.element.BossShip;
import fr.upemlv.escapeira.model.element.Bullet;
import fr.upemlv.escapeira.model.element.EnemyShipFactory;
import fr.upemlv.escapeira.model.element.EnnemyShip;
import fr.upemlv.escapeira.model.element.PlayerShip;
import fr.upemlv.escapeira.physic.World;
import fr.upemlv.escapeira.view.AbstractSurfaceView;

public class LevelPlay extends Level {
	private int duration;
	private int state;
	private World world;
	private int ships;
	private ArrayList<EnnemyShip> currentShips;
	private BossShip boss;
	private final PlayerShip player;
	private static final ArrayList<Bullet> bullets = new ArrayList<Bullet>();
	private static PointF currentPosition = new PointF();

	public LevelPlay(AbstractSurfaceView view, String name, int duration) {
		super(view, name, duration);

		this.state		= 1;// * 4;
		this.world = new World(new Vec2(0, 0));
		this.player = new PlayerShip(50);
		this.ships = 10;
		this.currentShips = new ArrayList<EnnemyShip>();
	}

	public static ArrayList<Bullet> getBullets() {
		return bullets;
	}

	public static PointF getPosition(){
		return currentPosition;
	}

	public int getState() {
		return this.state;
	}
	
	public boolean isOver(){
		if(!boss.isAlive())
			return true;
		return false;
	}

	public void fillCurrentShip(int ships){
		currentShips = EnemyShipFactory.buildShipArmy(ships);
	}

	public void update() {

		ArrayList<AbstractElement> toDelete = new ArrayList<AbstractElement>();
		World.WORLD.step(60, 8, 6);
		World.WORLD.clearForces();
		this.getBackground().update();
		this.player.update();
		if (this.player.getLife() < 0)
			return;
		if(currentShips.isEmpty() && this.ships != 0){
			this.fillCurrentShip(this.ships);
		}else{
			for (EnnemyShip ship : currentShips) {
				if(!ship.isAlive()) {
					this.player.getWeapons().get(ship.getSelectedWeapon().getId()).addAmmo(new Random().nextInt(ship.getInitialLife() + 1));
					World.WORLD.destroyBody(ship.getBody());
					toDelete.add(ship);
					this.ships--;
				}
				ship.update();
			}
			currentShips.removeAll(toDelete);
		}
		for (Bullet bullet : bullets) {
			if(bullet.getHit()){
				World.WORLD.destroyBody(bullet.getBody());
				toDelete.add(bullet);
			}
			bullet.update();
		}
		
		bullets.removeAll(toDelete);
		currentPosition = World.toPixel(this.player.getBody().getPosition());
		if (this.boss != null) {
			this.boss.update();
			if (!this.boss.isAlive()) {
				World.WORLD.destroyBody(this.boss.getBody());
			}
		}
		if (!this.player.isAlive())
			return;
		this.duration--;
		if (this.duration <= 0)
			this.state = 0;
	}

	public void render(Canvas canvas) {
		this.getBackground().render(canvas);
		this.player.render(canvas);
		for (Bullet bullet : bullets) {
			bullet.render(canvas);
		}
		for ( EnnemyShip ship : currentShips) {
			if(!ship.isAlive()){
				canvas.drawColor(Color.WHITE);
			}
			ship.render(canvas);
		}
		if (this.boss != null && this.boss.isAlive())
			this.boss.render(canvas);
	}

	public int getShips() {
		return this.ships;
	}

	public void setBoss(BossShip boss) {
		if (this.boss != null)
			return;
		this.boss = boss;
	}

	public BossShip getBoss() {
		return this.boss;
	}
	public PlayerShip getPlayer() {
		return player;
	}
}
