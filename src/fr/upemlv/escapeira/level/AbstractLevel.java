package fr.upemlv.escapeira.level;

import java.util.ArrayList;
import java.util.Random;

import org.jbox2d.common.Vec2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.view.Display;
import android.view.WindowManager;
import fr.upemlv.escapeira.EscapeIR;
import fr.upemlv.escapeira.R;
import fr.upemlv.escapeira.model.element.AbstractElement;
import fr.upemlv.escapeira.model.element.BossShip;
import fr.upemlv.escapeira.model.element.Bullet;
import fr.upemlv.escapeira.model.element.EnemyShipFactory;
import fr.upemlv.escapeira.model.element.EnnemyShip;
import fr.upemlv.escapeira.model.element.PlayerShip;
import fr.upemlv.escapeira.physic.World;
import fr.upemlv.escapeira.utils.DrawablesHolder;
import fr.upemlv.escapeira.utils.Renderable;
import fr.upemlv.escapeira.utils.Updatable;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 * This class represents a level
 * It contains everything you need to create a new level
 * The boss's properties are defined in the level class
 */

public abstract class AbstractLevel implements Renderable, Updatable {
	/**
	 * 
	 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
	 * This class represents the background of the game
	 */
	private class Background implements Updatable, Renderable {
		private static final int SCROLL_SPEED = 2;
		private static final int ROTATE_SPEED = 1;

		private final Bitmap backgroundImage;
		private int yScroll = 0;
		
		public Background() {
			this.backgroundImage 	= DrawablesHolder.getInstance().getImage(R.drawable.gamebg);
		}

		public void update() {
			this.yScroll += Background.SCROLL_SPEED;// * delta;
			if (this.yScroll > this.backgroundImage.getHeight())
				this.yScroll = 0;
		}

		public void render(Canvas canvas) {
			WindowManager wm = (WindowManager) EscapeIR.getContext().getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();

			for (int x = 0 ; x <= display.getWidth() ; x += this.backgroundImage.getWidth()) {
				for (int y = -this.backgroundImage.getHeight() ; y <= display.getHeight() ; y += this.backgroundImage.getHeight()) {
					canvas.drawBitmap(this.backgroundImage, x, y + this.yScroll, null);
				}
			}
		}
	}

	private static final int DURATION = 60;

	private final Background background;
	private int duration;
	private int state;
	@SuppressWarnings("unused")
	private World world;
	private int ships;
	private ArrayList<EnnemyShip> currentShips;
	private BossShip boss;
	private final PlayerShip player;
	private static final ArrayList<Bullet> bullets = new ArrayList<Bullet>();
	private static PointF currentPosition = new PointF();

	/**
	 * 
	 * @param planet
	 * @param difficulty
	 * Creates a level depending on a difficulty and a planet
	 * The difficulty will determine the number of life the enemies have and the planet will determine the background.
	 * 
	 */
	public AbstractLevel() {
		this.background = new Background();
		this.duration 	= AbstractLevel.DURATION*100;
		this.state		= 1;// * 4;
		
		this.world = new World(new Vec2(0, 0));
		this.player = new PlayerShip(50);
		this.ships = 10;
		this.currentShips = new ArrayList<EnnemyShip>();
		
	}

	public static ArrayList<Bullet> getBullets() {
		return bullets;
	}

	/**
	 * 
	 * @return the current position of the player
	 */
	public static PointF getPosition(){
		return currentPosition;
	}

	public int getState() {
		return this.state;
	}

	
	
	
	public boolean isOver(){
		if(!boss.isAlive())
			return true;
		return false;
	}

	/**
	 * This method create a new army of ship the hero has to kill.
	 */
	public void fillCurrentShip(int ships){
		currentShips = EnemyShipFactory.buildShipArmy(ships);
	}

	public void update() {

		ArrayList<AbstractElement> toDelete = new ArrayList<AbstractElement>();
		World.WORLD.step(60, 8, 6);
		World.WORLD.clearForces();
		this.background.update();
		this.player.update();
		if (this.player.getLife() < 0)
			return;
		if(currentShips.isEmpty() && this.ships != 0){
			this.fillCurrentShip(this.ships);
		}else{
			for (EnnemyShip ship : currentShips) {
				if(!ship.isAlive()) {
					this.player.getWeapons().get(ship.getSelectedWeapon().getId()).addAmmo(new Random().nextInt(ship.getInitialLife() + 1));
					World.WORLD.destroyBody(ship.getBody());
					toDelete.add(ship);
					this.ships--;
				}
				ship.update();
			}
			currentShips.removeAll(toDelete);
		}
		for (Bullet bullet : bullets) {
			if(bullet.getHit()){
				World.WORLD.destroyBody(bullet.getBody());
				toDelete.add(bullet);
			}
			if(!bullet.getBody().isAwake()){
				World.WORLD.destroyBody(bullet.getBody());
				toDelete.add(bullet);
			}
				
			bullet.update();
		}
		
		bullets.removeAll(toDelete);
		currentPosition = World.toPixel(this.player.getBody().getPosition());
		if (this.boss != null) {
			this.boss.update();
			if (!this.boss.isAlive()) {
				World.WORLD.destroyBody(this.boss.getBody());
			}
		}
		if (!this.player.isAlive())
			return;
		this.duration--;
		if (this.duration <= 0)
			this.state = 0;
	}

	public void render(Canvas canvas) {
		this.background.render(canvas);
		this.player.render(canvas);
		for (Bullet bullet : bullets) {
			bullet.render(canvas);
		}
		for ( EnnemyShip ship : currentShips) {
			if(!ship.isAlive()){
				canvas.drawColor(Color.WHITE);
			}
			ship.render(canvas);
		}
		if (this.boss != null && this.boss.isAlive())
			this.boss.render(canvas);
	}

	public int getShips() {
		return this.ships;
	}

	public void setBoss(BossShip boss) {
		if (this.boss != null)
			return;
		this.boss = boss;
	}

	public BossShip getBoss() {
		return this.boss;
	}
	public PlayerShip getPlayer() {
		return player;
	}
	
}
