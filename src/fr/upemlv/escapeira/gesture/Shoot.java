package fr.upemlv.escapeira.gesture;

import java.util.ArrayList;

import android.graphics.PointF;

import fr.upemlv.escapeira.gesture.GestureRecognizer.GestureID;

public class Shoot extends TranslationGesture {
	/**
	 * defines the ID of the gesture.
	 */
	public Shoot() {
		super(GestureID.SHOOT);
	}
	/**
	 * Recognize a straight line from a top point to a bottom point.
	 */
	@Override
	public boolean recognized(ArrayList<PointF> points) {
		return super.recognized(points);
	}
}
