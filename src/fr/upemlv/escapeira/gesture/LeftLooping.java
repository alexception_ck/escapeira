package fr.upemlv.escapeira.gesture;
import java.util.ArrayList;
import android.graphics.*;
import fr.upemlv.escapeira.gesture.GestureRecognizer.GestureID;

public class LeftLooping extends CircleGesture {
	public LeftLooping() {
		super(GestureID.LEFT_LOOPING);
	}

	@Override
	public boolean recognized(ArrayList<PointF> points) {
		if (points.size() < 4)
			return false;
		int multiple = (int) Math.floor((points.size() - 1) / 4);
		PointF reference = points.get(0);
		PointF p1 = points.get(multiple);
		PointF p2 = points.get(multiple * 2);
		PointF p3 = points.get(multiple * 3);
		return super.recognized(points) && CircleGesture.isCounterClockwise(reference, p1, p2, p3);
	}
}
