package fr.upemlv.escapeira.gesture;

import java.util.ArrayList;

import android.graphics.PointF;
import fr.upemlv.escapeira.gesture.GestureRecognizer.GestureID;
import fr.upemlv.escapeira.utils.PointMath;

public abstract class TranslationGesture extends Gesture {
	private Orientation orientation;

	public TranslationGesture(GestureID id) {
		super(id);
	}

	public Orientation getOrientation() {
		return this.orientation;
	}

	@Override
	public boolean recognized(ArrayList<PointF> points) {
		if (points.size() < 10)
			return false;

		final PointF startingPoint = points.get(0);
		final PointF endingPoint = points.get(points.size() - 1);
		this.orientation = Gesture.orientation(startingPoint, endingPoint);

		for (int i = 1 ; i < (points.size()-1) ; i++) {
			final double distance = PointMath.ptLineDist(startingPoint, endingPoint, points.get(i));
			if (distance > 10)
				return false;
		}

		return true;
	}
}
