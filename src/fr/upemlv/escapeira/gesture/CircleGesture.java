package fr.upemlv.escapeira.gesture;

import java.util.ArrayList;

import android.graphics.PointF;


import fr.upemlv.escapeira.GameActivity;
import fr.upemlv.escapeira.gesture.GestureRecognizer.GestureID;
import fr.upemlv.escapeira.utils.PointMath;

public abstract class CircleGesture extends Gesture {
	private static final int JOIN_SHAKINESS = (int) ((GameActivity.diagonale * 25) / 100);
	private static final int MINIMAL_RADIUS = (int) (GameActivity.diagonale / 35);
	private static final int MAXIMAL_RADIUS = (int) (GameActivity.diagonale / 15);
	
	/**
	 * defines the ID of the gesture.
	 */
	public CircleGesture(GestureID id) {
		super(id);
	}
	/**
	 * Recognize a circle. it tests if the first and last PointF are close enough then it finds the largest distance between two PointFs.
	 * This line will be used as a diameter for the circle, if the PointFs draw by the user are not too far away from this circle the gesture will be recognized.
	 * @param ArrayList<PointF>
	 * @return boolean
	 *  
	 */
	
	@Override
	public boolean recognized(ArrayList<PointF> PointFs) {
		if (PointFs.size() < 2)
			return false;
		
		if ((float)PointMath.distance(PointFs.get(0), PointFs.get(PointFs.size() - 1)) > CircleGesture.JOIN_SHAKINESS)
			return false;

		PointF farestPointF = PointFs.get(0);
		for (PointF pointF : PointFs)
			farestPointF = ((float) PointMath.distance(pointF, PointFs.get(0)) > (float)PointMath.distance(pointF, farestPointF)) ? pointF : farestPointF;

		PointF center = CircleGesture.segmentMiddle(PointFs.get(0), farestPointF);
		float radius = (float) PointMath.distance(center, PointFs.get(0));
		
		if (radius < CircleGesture.MINIMAL_RADIUS || radius > CircleGesture.MAXIMAL_RADIUS)
			return false;

		for (PointF pointF : PointFs) {
			if (PointMath.distance(center, pointF) < (radius - Gesture.shakiness))
				return false;
			if (PointMath.distance(center, pointF) > (radius + Gesture.shakiness))
				return false;
		}

		return true;
	}
	/**
	 * 
	 * This method determines the circle's center.
	 * @param PointF1
	 * @param PointF2
	 * @return a PointF
	 */
	public static PointF segmentMiddle(PointF PointF1, PointF PointF2) {
		int x = (int) ((PointF1.x + PointF2.x) / 2);
		int y = (int) ((PointF1.y + PointF2.y) / 2);
		return new PointF(x, y);
	}

	/**
	 * @param PointF reference
	 * @param PointF p1
	 * @param PointF p2
	 * @param PointF p3
	 * @return true if the circle is clockwise false other wise
	 */
	
	public static boolean isClockwise(PointF reference, PointF p1, PointF p2, PointF p3) {
		return ((Gesture.isNorthWest(reference, p1) && Gesture.isNorth(reference, p2) && Gesture.isNorthEast(reference, p3))
				|| (Gesture.isNorthEast(reference, p1) && Gesture.isEast(reference, p2) && Gesture.isSouthEast(reference, p3))
				|| (Gesture.isSouthEast(reference, p1) && Gesture.isSouth(reference, p2) && Gesture.isSouthWest(reference, p3))
				|| (Gesture.isSouthWest(reference, p1) && Gesture.isWest(reference, p2) && Gesture.isNorthWest(reference, p3)));
	}


	/**
	 * @param PointF reference
	 * @param PointF p1
	 * @param PointF p2
	 * @param PointF p3
	 * @return true if the circle is counterclockwise false other wise
	 */
	
	public static boolean isCounterClockwise(PointF reference, PointF p1, PointF p2, PointF p3) {
		return CircleGesture.isClockwise(reference, p3, p2, p1);
	}
}
