package fr.upemlv.escapeira.gesture;


import java.util.ArrayList;

import android.graphics.PointF;
import fr.upemlv.escapeira.GameActivity;
import fr.upemlv.escapeira.gesture.GestureRecognizer.GestureID;
/**
 * 
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public abstract class Gesture {
	private final GestureID id;
	protected static final double shakiness = (int) (GameActivity.diagonale* 25) / 100;

	/**
	 * Enumeration of the different orientation of a gesture
	 */
	
	public enum Orientation {
		UNKNOWN,
		NORTH,
		SOUTH,
		EAST,
		WEST,
		NORTH_WEST,
		NORTH_EAST,
		SOUTH_WEST,
		SOUTH_EAST,
	}

	public Gesture(GestureID id) {
		this.id = id;
	}

	public GestureID getId() {
		return this.id;
	}

	/**
	 * implements this gesture in a new Gesture class to recognized it.
	 * @param PointFs
	 * @return true if the gesture is recognized.
	 */
	public abstract boolean recognized(ArrayList<PointF> PointFs);
	/**
	 * 
	 * @param PointF
	 * @param from
	 * 
	 * @return true if the orientation is south 
	 */
	public static boolean isSouth(PointF PointF, PointF from) {
		return Gesture.isSouth(PointF, from, false);
	}

	/**
	 * 
	 * @param reference
	 * @param PointF
	 * @return true if the PointF is not outside the tolerance zone of shakiness
	 */
	public static boolean isInShakinessZone(double reference, double PointF) {
		return PointF > (reference - Gesture.shakiness) && PointF < (reference + Gesture.shakiness);
	}
	/**
	 * 
	 * @param PointF
	 * @param from
	 * @param shakinessCompliant
	 * 
	 * @return true if the orientation is south and the PointF is not outside the tolerance zone of shakiness 
	 */
	
	public static boolean isSouth(PointF PointF, PointF from, boolean shakinessCompliant) {
		boolean isSouth = PointF.y > from.y;
		if (!shakinessCompliant)
			return isSouth;

		return isSouth && Gesture.isInShakinessZone(from.x, PointF.x);
	}
	/**
	 * @return true if the orientation is north
	 */
	public static boolean isNorth(PointF PointF, PointF from) {
		return Gesture.isNorth(PointF, from, false);
	}
	/**
	 * 
	 * @param endingPointF
	 * @param startingPointF
	 * @param shakinessCompliant
	 * 
	 * @return true if the orientation is north and the PointF is not outside the tolerance zone of shakiness
	 */
	public static boolean isNorth(PointF endingPointF, PointF startingPointF, boolean shakinessCompliant) {
		boolean isNorth = !Gesture.isSouth(endingPointF, startingPointF);
		if (!shakinessCompliant)
			return isNorth;
		
		return isNorth && Gesture.isInShakinessZone(startingPointF.x, endingPointF.x);
	}
	/**
	 * 
	 * @param PointF
	 * @param from
	 * @return true if the orientation is West
	 */
	public static boolean isWest(PointF PointF, PointF from) {
		return PointF.x < from.x;
	}
	/**
	 * 
	 * @param PointF
	 * @param from
	 * @param shakinessCompliant
	 * 
	 * @return true if the orientation is west the PointF is not outside the tolerance zone of shakiness
	 */
	public static boolean isWest(PointF PointF, PointF from, boolean shakinessCompliant) {
		boolean isWest = PointF.x < from.x;
		if (!shakinessCompliant)
			return isWest;

		return isWest && Gesture.isInShakinessZone(from.y, PointF.y);
	}

	public static boolean isEast(PointF PointF, PointF from) {
		return Gesture.isEast(PointF, from, false);
	}

	/**
	 * 
	 * @param PointF
	 * @param from
	 * @param shakinessCompliant
	 * 
	 * @return true if the orientation is east and the PointF is not outside the tolerance zone of shakiness
	 */
	public static boolean isEast(PointF PointF, PointF from, boolean shakinessCompliant) {
		boolean isEast = !Gesture.isWest(PointF, from, false);
		if (!shakinessCompliant)
			return isEast;
		
		return isEast && Gesture.isInShakinessZone(from.y, PointF.y);
	}

	public static boolean isNorthWest(PointF PointF, PointF from) {
		return Gesture.isNorth(PointF, from) && Gesture.isWest(PointF, from);
	}

	public static boolean isNorthEast(PointF PointF, PointF from) {
		return Gesture.isNorth(PointF, from) && Gesture.isEast(PointF, from);
	}

	public static boolean isSouthWest(PointF PointF, PointF from) {
		return Gesture.isNorthEast(from, PointF);
	}

	public static boolean isSouthEast(PointF PointF, PointF from) {
		return Gesture.isNorthWest(from, PointF);
	}
	/**
	 * 
	 * @param startingPointF
	 * @param endingPointF
	 * @return the orientation of line beetween 2 PointFs
	 */
	public static Orientation orientation(PointF startingPointF, PointF endingPointF) {
		if (isNorth(endingPointF, startingPointF, true))
			return Orientation.NORTH;
		if (isWest(endingPointF, startingPointF, true))
			return Orientation.WEST;
		if (isSouth(endingPointF, startingPointF, true))
			return Orientation.SOUTH;
		if (isEast(endingPointF, startingPointF, true))
			return Orientation.EAST;

		if (isNorthWest(endingPointF, startingPointF))
			return Orientation.NORTH_WEST;
		if (isSouthWest(endingPointF, startingPointF))
			return Orientation.SOUTH_WEST;
		if (isSouthEast(endingPointF, startingPointF))
			return Orientation.SOUTH_EAST;
		if (isNorthEast(endingPointF, startingPointF))
			return Orientation.NORTH_EAST;

		return Orientation.UNKNOWN;
	}
	/**
	 * 
	 * @param from
	 * @param to
	 * @return an int which represent the angle of line between 2 PointFs
	 */
	public static int getAngle(PointF from, PointF to) {
		int angle = (int) Math.toDegrees(Math.atan2(to.y - from.y, to.x - from.x));
		return (angle < 0) ? angle + 360 : angle; 
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @return a double which represents the slope of a line between 2 PointFs
	 */
	public static double slope(PointF from, PointF to) {
		return (to.y - from.y) / (to.x - from.x);
	}
}
