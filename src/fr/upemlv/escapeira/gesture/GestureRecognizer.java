package fr.upemlv.escapeira.gesture;

import java.util.ArrayList;
import java.util.LinkedList;

import org.jbox2d.common.Vec2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.Log;

import fr.upemlv.escapeira.physic.World;
import fr.upemlv.escapeira.utils.PointMath;
import fr.upemlv.escapeira.utils.Renderable;


/**
 * Handle gestures catching and recognition
 * A gesture being a movement starting when the left-click is pressed and ending when the left click is released
 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
 *
 */
public final class GestureRecognizer implements Renderable {
	/**
	 * Enumeration representing types of gestures
	 * @author Joachim ARCHAMBAULT, Alexandre ANDRE
	 */
	public enum GestureID {
		/**
		 * A simple movement on the left and slightly forward
		 */
		LEFT_DRIFT,
		/**
		 * A simple movement on the left and slightly forward
		 */
		RIGHT_DRIFT,
		/**
		 * A simple backward movement (kind of defensive)
		 */
		SHOOT,
		/**
		 * A forward movement with shoot
		 */
		ATTACK,
		/**
		 * A simple shoot
		 */
		MOVE,
		/**
		 * A looping on the left
		 */
		LEFT_LOOPING,
		/**
		 * A looping on the right
		 */
		RIGHT_LOOPING,
		/**
		 * Represents an invalid gesture
		 */
		INVALID,
		/**
		 * Represents an unknown or unfinished gesture
		 */
		UNKNOWN,
		/**
		 * A click to pick a weapon
		 */
		SELECT,
	}

	private static GestureRecognizer instance = null;
	private boolean capturing;
	private GestureID recognizedGesture;
	private GestureID drawedRecognizedGesture;
	private ArrayList<PointF> points;
	private LinkedList<Gesture> gestures;
	private Gesture recognizedGestureObject;
	private boolean up;
	private boolean down;
	private PointF last;
	
	private GestureRecognizer() {
		this.capturing = false;
		this.setGesture(GestureID.UNKNOWN);
		this.points = new ArrayList<PointF>();
		this.gestures = new LinkedList<Gesture>();
		this.gestures.add(new Shoot());
		this.gestures.add(new LeftLooping());
		this.gestures.add(new RightLooping());
		this.gestures.add(new Select());
	}

	/**
	 * Returns the unique instance of GestureRecognizer
	 * @return an instance of GestureRecognizer
	 */
	public static GestureRecognizer getInstance() {
		if (GestureRecognizer.instance == null)
			GestureRecognizer.instance = new GestureRecognizer();

		return GestureRecognizer.instance;
	}

	/**
	 * Update the point list representing a possible gesture
	 * @param context The application context to handle event
	 */
	public void update(float x, float y) {
			PointF point = new PointF(x, y);
			points.add(point);
	}
	/**
	 * Analyze the list of points and try to define a gesture
	 */
	public void analyze() {
		this.recognizedGestureObject = null;
		this.setGesture(GestureID.INVALID);
		for (Gesture gesture : this.gestures) {
			if (!gesture.recognized(this.points))
				continue;
			this.recognizedGestureObject = gesture;
			this.setGesture(gesture.getId());
			return;
		}
	}

	/**
	 * Draw the gesture on the screen
	 */
	public void render(Canvas canvas) {
		@SuppressWarnings("unchecked")
		ArrayList<PointF> tmp = (ArrayList<PointF>) points.clone();
		if(this.down){
			
			this.points.clear();
			tmp.clear();
			canvas.restore();
			canvas.save();
			this.down = false;
		}
		Paint paint = new Paint();	
		if (this.points.size() < 2)
			return;
		this.last = this.points.get(0);
		for (PointF point : tmp) {
			if (this.drawedRecognizedGesture == GestureID.UNKNOWN) {
				paint.setColor(Color.WHITE);
			}
			else if (this.drawedRecognizedGesture == GestureID.INVALID){
				paint.setColor(Color.RED);
			} else {
				paint.setColor(Color.GREEN);
			}
			//if(this.drawedRecognizedGesture != GestureID.MOVE)
			canvas.drawLine(last.x, last.y, point.x, point.y, paint);
			this.last = points.get(tmp.lastIndexOf(point));
		}
	}

	/**
	 * Return the analyzed gesture
	 * @return the gesture identifier
	 */
	public GestureID getGesture() {
		return this.recognizedGesture;
	}

	public Gesture getGestureObject() {
		return this.recognizedGestureObject;
	}

	public PointF getLast(){
		return this.last;
	}
	
	/**
	 * Clear the analyzed gesture
	 */
	public void clearGesture() {
		this.recognizedGesture = GestureID.UNKNOWN;
		this.recognizedGestureObject = null;
	}
	
	public void down(){
		this.down = true;
		this.up = false;
		this.setGesture(GestureID.UNKNOWN);
		this.recognizedGestureObject = null;
	}

	public void up() {
		this.down = false;
		this.up = true;
		this.recognizedGestureObject = null;
	}
	
	private void setGesture(GestureID gesture) {
		this.recognizedGesture = gesture;
		this.drawedRecognizedGesture = gesture;
	}

	public PointF getStartingPoint() {
		return this.points.get(0);
	}

	public PointF getEndingPoint() {
		return this.points.get(this.points.size() - 1);
	}

	public ArrayList<PointF> getPoints(){
		return this.points;
	}
	
	/**
	 * Returns the distance between the first and last stored points
	 * @return distance between first and last point
	 */
	public double getDistance() {
		if (this.points.size() < 2)
			return 0;
		return PointMath.distance(getStartingPoint(), getEndingPoint());
	}

	/**
	 * Returns the distance angle the first and last stored points
	 * @return angle between first and last point
	 */
	public Vec2 getAngle() {
		if (this.points.size() < 2)
			return new Vec2(0, 0);

		return World.angle(this.getStartingPoint(), this.getEndingPoint());
	}
}
