package fr.upemlv.escapeira.gesture;

import android.graphics.PointF;
import java.util.ArrayList;


import fr.upemlv.escapeira.gesture.GestureRecognizer.GestureID;

public class Select extends Gesture{

	public enum Selected{
		MISSIL,
		FIRE,
		SHIBOOLET,
		UKNOWN,
	}
	
	public Select() {
		super(GestureID.SELECT);
	}

	@Override
	public boolean recognized(ArrayList<PointF> points) {
		return points.size() < 5 && points.size() > 0;
	}
	
}
