package fr.upemlv.escapeira.gesture;

import android.graphics.*;
import java.util.ArrayList;
import fr.upemlv.escapeira.gesture.GestureRecognizer.GestureID;
import fr.upemlv.escapeira.level.AbstractLevel;
import fr.upemlv.escapeira.model.element.PlayerShip;
import fr.upemlv.escapeira.utils.PointMath;

public class Move extends TranslationGesture{
	public Move() {
		super(GestureID.MOVE);
	}

	@Override
	public boolean recognized(ArrayList<PointF> points) {
		return true;
	}
}